<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date')->nullable();

            $table->double('distance', 5, 2)->nullable();
            $table->time('duration')->nullable();
            $table->time('duration_moved')->nullable();
            $table->integer('calories')->nullable();

            $table->double('elevation', 7, 2)->nullable();

            $table->double('if', 5, 2)->nullable();
            $table->double('tss', 5, 2)->nullable();
            $table->double('vi', 5, 2)->nullable();

            $table->integer('hrt_avg')->nullable();
            $table->integer('hrt_max')->nullable();
            $table->text('hrt_part')->nullable();

            $table->double('spd_avg', 5, 2)->nullable();
            $table->double('spd_max', 5, 2)->nullable();

            $table->integer('cad_avg')->nullable();
            $table->integer('cad_max')->nullable();

            $table->integer('pwr_avg')->nullable();
            $table->integer('pwr_max')->nullable();
            $table->integer('pwr_np')->nullable();
            $table->integer('pwr_ftp')->nullable();
            $table->text('pwr_part')->nullable();

            $table->text('pwr_critical')->nullable();
            $table->text('pwr_histogram')->nullable();

            $table->longText('line_spd')->nullable();
            $table->longText('line_pwr')->nullable();
            $table->longText('line_hrt')->nullable();
            $table->longText('line_cad')->nullable();
            $table->longText('line_alt')->nullable();

            $table->longText('cords')->nullable();
            $table->string('static_map')->nullable();

            $table->integer('user_id')->unsigned()->nullable();
            $table->boolean('override')->default(0);
            $table->string('filename')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('activity');
    }
}
