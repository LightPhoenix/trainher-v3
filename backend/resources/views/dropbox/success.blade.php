<html>
<head>
    <title>TrainHer - Dropbox authorisation successful</title>
</head>
<body>

<div style="text-align: center; font-family: Helvetica; font-size: 16px; color: rgba(0,0,0,0.7)">
    <img src="images/trainher.svg" width="150px">
    <br><br>
    Dropbox authorisation successful!
</div>

</body>
</html>