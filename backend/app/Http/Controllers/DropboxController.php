<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 05-06-18
 * Time: 22:07
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Session;
use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\Dropbox;

use App\Http\Models\Activity;
use App\Http\Models\AnalyseFitFile;


class DropboxController extends Controller
{

    public function authoriseAccount($userHash = null)
    {
        $sAuthorisationFailed = view('dropbox.failed');
        $sAuthorisationSuccess = view('dropbox.success');

        if($userHash != null) {
            $oUser = User::where('user_hash', '=', $userHash)->first();

            if(!$oUser) {
                return $sAuthorisationFailed;
            } else {
                if(isset($_GET['return_url'])) {
                    $oUser->dropbox_return = $_GET['return_url'];
                    $oUser->save();
                }
                Session::put('user_id', $oUser->id);
            }
        }

        $oDropboxApp = new DropboxApp(config('api.dropbox_client_id'), config('api.dropbox_client_secret'));
        $config = ['random_string_generator' => 'openssl'];

        $oDropbox = new Dropbox($oDropboxApp, $config);

        $authHelper = $oDropbox->getAuthHelper();
        $callbackUrl = config('api.dropbox_return_url'); //returnurl

        $authUrl = $authHelper->getAuthUrl($callbackUrl);

        if (isset($_GET['code']) && isset($_GET['state'])) {
            //Bad practice! No input sanitization!
            $code = $_GET['code'];
            $state = $_GET['state'];

            $oDropbox->getAuthHelper()->getPersistentDataStore()->set('state', filter_var($_GET['state'], FILTER_SANITIZE_STRING));

            try {
                //Fetch the AccessToken
                $accessToken = $authHelper->getAccessToken($code, $state, $callbackUrl);

                if(!Session::has('user_id'))
                    return $sAuthorisationFailed;

                $oUser = User::find(Session::get('user_id'));
                $oUser->token_dropbox = $accessToken->getToken();
                $oUser->save();

                if($oUser->dropbox_return != null)
                    return redirect()->away($oUser->dropbox_return);
                else
                    return $sAuthorisationSuccess;
            }
            catch (\Throwable | \Error | \Exception $e)
            {
                return $sAuthorisationFailed;
            }
        } else {
            return redirect()->away($authUrl);
        }

    }

    public function disconnectDropbox($userHash = null){
        if($userHash != null) {
            $oUser = User::where('user_hash', '=', $userHash)->first();

            if($oUser) {
                $oUser->token_dropbox = null;
                $oUser->save();

                return redirect()->away($oUser->dropbox_return);
            }
        }
    }

    public function getDroboxObject($sAT = '') {
        $app = new DropboxApp(config('api.dropbox_client_id'), config('api.dropbox_client_secret'), $sAT);

        $config = ['random_string_generator' => 'openssl'];

        return new Dropbox($app, $config);
    }

    public function getFiles($sAT = '') {

        $oDropbox = $this->getDroboxObject($sAT);

        $listFolderContents = $oDropbox->listFolder("/apps/wahoofitness");
        $items = $listFolderContents->getItems();

        return $items->all();
    }

    public function getFilesForAllUsers() {
        set_time_limit(0);
        $aUsers = User::whereNotNull('token_dropbox')->get();
        foreach($aUsers as $oUser) {
            $aFiles = $this->getFiles($oUser->token_dropbox);
            $oDropbox = $this->getDroboxObject($oUser->token_dropbox);

            foreach ($aFiles as $file) {
                $sName = str_replace('BOLT ', '', $file->getName()); //REMOVE BOLT otherwise there will be duplicates
                $sName = str_replace('ELEMNT ', '', $sName); //REMOVE ELMNT to shorten name
                $sPath = public_path() . '/uploads/activities/' . $sName;

                if(!file_exists($sPath)) {
                    $sFile = $oDropbox->download($file->path_lower);
                    file_put_contents($sPath, $sFile->getContents());

                    $oAnalysis = new AnalyseFitFile();
                    $aResult = $oAnalysis->analyseFile($sPath);
                    $aResult['user_id'] = $oUser->id;
                    $aResult['filename'] = $sName;

                    if((double)$aResult['distance'] > 0)
                        Activity::updateOrCreate($aResult, ['filename' => $sName]);
                    else
                        unlink($sPath); //remove file with 0 distance
                }
            }
        }
    }


}