<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 7-11-2018
 * Time: 17:49
 */

namespace App\Http\Controllers;


use App\Http\Models\Activity;
use App\Http\Resources\SimpleActivityResource;
use Illuminate\Http\Request;

class ActivityController extends Controller
{

    public function index(Request $request) {
        $per_page = env('ACTIVITY_PER_PAGE', 10);
        return SimpleActivityResource::collection(Activity::where('user_id', '=', $request->user()->id)->orderBy('date', 'desc')->paginate($per_page));
    }

}