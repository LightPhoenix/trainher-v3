<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 5-11-2018
 * Time: 22:36
 */

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $guarded = ['id'];
    protected $table = 'activity';
    public $timestamps = true;

}