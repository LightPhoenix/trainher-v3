<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 2-11-2018
 * Time: 08:31
 */

namespace App\Http\Models;

use Storage;

use PointReduction\Common\Point,
    PointReduction\Algorithms\VisvalingamWhyatt;

class StaticMap
{

    static function getRouteMap($coordinates){

        if(count($coordinates) > env('STATIC_LIMIT', 1500)) {

            $givenPoints = array();

            foreach ($coordinates as $point) {
                $givenPoints[] = new Point($point[0], $point[1]);
            }

            $desiredPointCount = env('STATIC_LIMIT', 1500);
            $reducer = new VisvalingamWhyatt($givenPoints);
            $reducedPoints = $reducer->reduce($desiredPointCount);

            $useCords = [];

            foreach ($reducedPoints as $point) {
                $useCords[] = [$point->x, $point->y];
            }
        } else {
            $useCords = $coordinates;
        }

        $coordinates = self::encodeGPolyline($useCords);

        $url = "http://maps.googleapis.com/maps/api/staticmap?sensor=false&size=" .
            env('STATIC_WIDTH') . "x" . env('STATIC_HEIGHT') .
            "&path=weight:" . env('STATIC_LINE_WEIGHT') . "|color:" . env('STATIC_LINE_COLOR') . "|enc:" . $coordinates .
            "&key=" . env('STATIC_KEY');

        $contents = file_get_contents($url);
        $filename = md5($url . microtime()) . ".png";

        Storage::disk('static-map')->put($filename, $contents);

        return url('/') . '/uploads/static-map/' . $filename;
    }

    static function encodeGPolyline($path)
    {
        $enc = "";
        $old = true;
        foreach ($path as $latlng) {
            if ($old === true) {
                $enc .= self::encodeGPolylineNum($latlng[0]) .
                    self::encodeGPolylineNum($latlng[1]);
            } else {
                $enc .= self::encodeGPolylineNum($latlng[0] - $old[0]) .
                    self::encodeGPolylineNum($latlng[1] - $old[1]);
            }
            $old = $latlng;
        }
        return $enc;
    }

    static function encodeGPolylineNum($num)
    {
        $fu = false;
        if ($num < 0) {
            $fu = true;
        }
        $num = round($num * 100000);
        $num = $num << 1;
        if ($fu) {
            $num = ~$num;
        }
        $num = decbin($num);
        $n = str_split($num);
        $num = array();
        $nn = "";
        for ($c = count($n) - 1; $c >= 0; $c--) {
            $nn = $n[$c] . $nn;
            if (strlen($nn) == 5) {
                array_push($num, $nn);
                $nn = "";
            }
        }
        if (strlen($nn) > 0) {
            $nn = str_repeat("0", 5 - strlen($nn)) . $nn;
            array_push($num, $nn);
        }

        for ($c = 0; $c < count($num); $c++) {
            if ($c != count($num) - 1) {
                $num[$c] = chr(bindec($num[$c]) + 32 + 63);
            } else {
                $num[$c] = chr(bindec($num[$c]) + 63);
            }
        }
        return implode("", $num);
    }
}