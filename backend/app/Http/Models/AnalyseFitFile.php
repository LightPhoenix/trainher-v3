<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 2-11-2018
 * Time: 08:38
 */

namespace App\Http\Models;

use adriangibbons\phpFITFileAnalysis;
use App\Http\Models\StaticMap;

class AnalyseFitFile
{

    public static function getData($sFile = '')
    {
        $options = ['data_every_second' => false, 'fix_data' => ['all']];

        $oFFA = new phpFITFileAnalysis($sFile, $options);

        return $oFFA;
    }

    public function analyseFile($sPath) {
        $oAnalysis = $this->getData($sPath);

        $sDistance = $oAnalysis->data_mesgs['session']['total_distance'];
        $sDuration = gmdate('H:i:s', $oAnalysis->data_mesgs['session']['total_elapsed_time']);
        $sDurationMoved = gmdate('H:i:s', $oAnalysis->data_mesgs['session']['total_timer_time']);

        $sCalories = null;

        if(isset($oAnalysis->data_mesgs['session']['total_calories']))
            $sCalories = $oAnalysis->data_mesgs['session']['total_calories'];

        $aLineSpeed = $oAnalysis->data_mesgs['record']['speed'];
        $aLineAltitude = $oAnalysis->data_mesgs['record']['altitude'];

        $aCriticalPower = null;
        $sNp = null;
        $aHistogram = null;
        $iFtp = null;
        $sAvgPower = null;
        $sMaxPower = null;
        $aLinePower = null;
        $aPowerPartitioned = null;
        if(isset($oAnalysis->data_mesgs['session']['avg_power'])) {
            $sAvgPower = $oAnalysis->data_mesgs['session']['avg_power'];
            $sMaxPower = $oAnalysis->data_mesgs['session']['max_power'];
            $iFtp = $oAnalysis->data_mesgs['session']['threshold_power'];
            $aCriticalPower = $oAnalysis->criticalPower([2,3,5,10,30,60,120,300,600,1200,3600,7200,10800,18000]);
            $aPowerPartitioned = $oAnalysis->data_mesgs['session']['time_in_power_zone'];
            $sNp = $oAnalysis->data_mesgs['session']['normalized_power'];
            $aHistogram = $oAnalysis->powerHistogram();
            $aLinePower = $oAnalysis->data_mesgs['record']['power'];
        }

        $sAvgHeartRate = null;
        $sMaxHeartRate = null;
        $aLineHeartRate = null;
        $aHeartRatePartitioned = null;
        if(isset($oAnalysis->data_mesgs['session']['avg_heart_rate'])) {
            $sAvgHeartRate = $oAnalysis->data_mesgs['session']['avg_heart_rate'];
            $aLineHeartRate = $oAnalysis->data_mesgs['record']['heart_rate'];
            $sMaxHeartRate = $oAnalysis->data_mesgs['session']['max_heart_rate'];
            $aHeartRatePartitioned = $oAnalysis->data_mesgs['session']['time_in_hr_zone'];
        }

        $sAvgCadence = null;
        $sMaxCadence = null;
        $aLineCadence = null;
        if(isset($oAnalysis->data_mesgs['session']['avg_cadence'])) {
            $sAvgCadence = $oAnalysis->data_mesgs['session']['avg_cadence'];
            $sMaxCadence = $oAnalysis->data_mesgs['session']['max_cadence'];
            $aLineCadence = $oAnalysis->data_mesgs['record']['cadence'];
        }

        $intensity_factor = null;
        if(isset($oAnalysis->data_mesgs['session']['intensity_factor']))
            $intensity_factor = $oAnalysis->data_mesgs['session']['intensity_factor'];

        $training_stress_score = null;
        if(isset($oAnalysis->data_mesgs['session']['training_stress_score']))
            $training_stress_score = $oAnalysis->data_mesgs['session']['training_stress_score'];

        $total_ascent = $oAnalysis->data_mesgs['session']['total_ascent'];
        $sAvgSpeed = $oAnalysis->data_mesgs['session']['avg_speed'];
        $sMaxSpeed = $oAnalysis->data_mesgs['session']['max_speed'];

        $aCords = [];
        if(isset($oAnalysis->data_mesgs['record']['position_lat']))
        {
            $aLon = $oAnalysis->data_mesgs['record']['position_long'];
            $aLat = $oAnalysis->data_mesgs['record']['position_lat'];

            foreach ($aLon as $key => $value)
            {
                $aCords[] = [$aLat[$key], $value];
            }
        }

        return [
            'date' => date('Y-m-d H:i:s', $oAnalysis->data_mesgs['session']['start_time']),
            'distance' => $sDistance,
            'duration' => $sDuration,
            'duration_moved' => $sDurationMoved,
            'calories' => $sCalories,

            'if' => $intensity_factor,
            'tss' => $training_stress_score,
            'elevation' => $total_ascent,

            'hrt_avg' => $sAvgHeartRate,
            'hrt_max' => $sMaxHeartRate,
            'hrt_part' => json_encode($aHeartRatePartitioned),

            'cad_avg' => $sAvgCadence,
            'cad_max' => $sMaxCadence,

            'spd_avg' => round($sAvgSpeed, 1),
            'spd_max' => round($sMaxSpeed, 1),

            'line_hrt' => json_encode($aLineHeartRate),
            'line_cad' => json_encode($aLineCadence),
            'line_pwr' => json_encode($aLinePower),
            'line_spd' => json_encode($aLineSpeed),
            'line_alt' => json_encode($aLineAltitude),
            'cords' => json_encode($aCords),
            'static_map' => StaticMap::getRouteMap($aCords),

            'pwr_avg' => $sAvgPower,
            'pwr_max' => $sMaxPower,
            'pwr_np' => $sNp,
            'pwr_ftp' => $iFtp,
            'pwr_part' => json_encode($aPowerPartitioned),
            'pwr_critical' => json_encode($aCriticalPower),
            'pwr_histogram' => json_encode($aHistogram),
        ];
    }

}