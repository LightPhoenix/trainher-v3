<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SimpleActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => date('d-m-Y H:i:s', strtotime($this->date)),
            'distance' => (double)$this->distance,
            'duration' => date('H:i:s', strtotime($this->duration)),
            'duration_moved' => date('H:i:s', strtotime($this->duration_moved)),
            'calories' => (int)$this->calories,
            'elevation' => (double)$this->elevation,
            'if' => (double)$this->if,
            'tss' => (double)$this->tss,
            'vi' => (double)$this->vi,
            'hrt_avg' => (int)$this->hrt_avg,
            'hrt_max' => (int)$this->hrt_max,
            'spd_avg' => (double)$this->spd_avg,
            'spd_max' => (double)$this->spd_max,
            'cad_avg' => (double)$this->cad_avg,
            'cad_max' => (double)$this->cad_max,
            'pwr_avg' => (int)$this->pwr_avg,
            'pwr_max' => (int)$this->pwr_max,
            'pwr_np' => (int)$this->pwr_np,
            'pwr_ftp' => (int)$this->pwr_ftp,
            'static_map' => $this->static_map,
            'created_at' => date('d-m-Y H:i:s', strtotime($this->created_at)),
            'updated_at' => date('d-m-Y H:i:s', strtotime($this->updated_at)),
        ];
    }
}

