<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $appends = ['dropbox_connected', 'dropbox_url', 'dropbox_url_disconnect'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getDropboxConnectedAttribute()
    {
        return ($this->token_dropbox) ? true : false;
    }

    public function getDropboxUrlAttribute()
    {
        return route('dropbox.auth', $this->user_hash);
    }

    public function getDropboxUrlDisconnectAttribute()
    {
        return route('dropbox.disconnect', $this->user_hash);
    }

    public static function getUserIdByToken($token)
    {
        $obj = self::where('token_api', '=', $token)->first();
        return $obj->id;
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->user_hash = sha1($user->id);
            $user->save();
        });
    }
}
