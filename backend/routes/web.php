<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', 'TestController@test')->name('test');

Route::get('authoriseDropbox/{userHash?}', 'DropboxController@authoriseAccount')->name('dropbox.auth');
Route::get('disconnectDropbox/{userHash?}', 'DropboxController@disconnectDropbox')->name('dropbox.disconnect');
Route::get('getFilesFromDropbox', 'DropboxController@getFilesForAllUsers')->name('dropbox.files');