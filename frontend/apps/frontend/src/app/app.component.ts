import { Component, OnInit } from '@angular/core';
import { ActivityService } from 'libs/services/src/lib/http/activity.service';

@Component({
  selector: 'frontend-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'frontend';

  constructor(private activityService: ActivityService) {

  }

  async ngOnInit() {
    console.log(await this.activityService.getList());
  }

}
