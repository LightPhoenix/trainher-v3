import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'activity-item',
  templateUrl: './activity-item.component.html',
  styleUrls: ['./activity-item.component.scss']
})
export class ActivityItemComponent implements OnInit {

  @Input() image: string = "http://127.0.0.1:8000/uploads/static-map/9ab2c2eb1b2b1bfb7e66b423fcfe90e6.png";
  @Input() date: string = "02 november 2018 om 10:25";
  @Input() distance: string = "67,3";
  @Input() duration: string = "01:45:21";
  @Input() stress: number = 133;

  constructor() { }

  ngOnInit() {
  }

}
