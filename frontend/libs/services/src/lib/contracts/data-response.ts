export interface ListResponse<T> {
    data: Array<T>
}

export interface CollectionResponse<T> extends ListResponse<T> {
    links: {
        first: string,
        last: string,
        prev: string,
        next: string
    },
    meta: {
        current_page: number,
        from: number,
        last_page: number,
        path: string,
        per_page: number,
        to: number,
        total: number
    }
}

export interface ModelResponse<T> {
    data: T
}
