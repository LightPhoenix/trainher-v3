import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../http/auth.service";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { tap } from 'rxjs/operators'

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${this.auth.getToken()}`
              }
            });
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            // redirect to the login route
            // or show a modal
            console.log('TODO: implement 401 in interceptor')
          }
        }
      }));
  }
}