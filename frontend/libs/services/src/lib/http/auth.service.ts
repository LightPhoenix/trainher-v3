import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  public getToken(): string {
    return localStorage.getItem('auth_token');
  }

  public hasToken(): boolean {
    if(this.getToken()) {
      return true;
    }
    return false;
  }

  public setToken(value: string): void {
    localStorage.setItem('auth_token', value);
  }

}
