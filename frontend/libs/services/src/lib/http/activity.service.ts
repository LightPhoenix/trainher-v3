import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { CollectionResponse } from '../contracts/data-response';
import { Activity } from '../contracts/activity';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  path: string = 'activities';

  constructor(public http: HttpClient, protected env: EnvironmentService) { }

  async getList(): Promise<CollectionResponse<Activity>> {
    const resp = <CollectionResponse<Activity>>await this.http.get(`${this.env.baseUrl}/${this.path}`).toPromise();
    return resp;
  }

}
