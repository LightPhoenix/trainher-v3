import { Injectable, InjectionToken, Inject } from '@angular/core';

export const BASE_URL = new InjectionToken<string>('');

@Injectable({
  providedIn: 'root'
})

export class EnvironmentService {

  constructor(@Inject(BASE_URL) public baseUrl: string) { }
}
